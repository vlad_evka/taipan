#!/bin/bash

function abspath() {
  (
    if [ -d "$1" ]; then
        cd "$1" && pwd -P
    else
        echo "$(cd "$(dirname "$1")" && pwd -P)/$(basename "$1")"
    fi
  )
}

SCRIPT_DIR=$(dirname "$(abspath "${BASH_SOURCE[0]}")")

INC_FILES=$(find "$(abspath ${SCRIPT_DIR}/../include)" -name "*.h")
SRC_FILES=$(find "$(abspath ${SCRIPT_DIR}/../src)" -name "*.cxx")

TESTS_INC_FILES=$(find "$(abspath ${SCRIPT_DIR}/../tests)" -name "*.h")
TESTS_SRC_FILES=$(find "$(abspath ${SCRIPT_DIR}/../tests)" -name "*.cxx")

for f in ${INC_FILES} ${SRC_FILES} \
         ${TESTS_INC_FILES} ${TESTS_SRC_FILES}
do
    clang-format -style "Chromium" "$f" > "$f.formatted" && mv "$f.formatted" "$f"
done
