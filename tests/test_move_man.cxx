#include <vector>
#include <string>

#include "FEN.h"
#include "Generator.h"

#include "catch.hpp"

/*
Это цифры перебора всех позиций для русских шашек начиная со стандартной позиции

Perft

1 - 7
2 - 49
3 - 302
4 - 1469
5 - 7482
6 - 37986
7 - 190146
8 - 929905
9 - 4570667
10 - 22450628
11 - 110961169
12 - 545059387
13 - 2675994747
14 - 13138899366

Доска представлена mailbox

   0   1   2   3
 4   5   6   7   8
   9  10  11  12
13  14  15  16  17
  18  19  20  21
22  23  24  25  26
  27  28  29  30
31  32  33  34  35
  36  37  38  39
40  41  42  43  44

   g8  e8  c8  a8  1-4
 h7  f7  d7  b7    5-8
   g6  e6  c6  a6  9-12
 h5  f5  d5  b5    13-16
   g4  e4  c4  a4  17-20
 h3  f3  d3  b3    21-24
   g2  e2  c2  a2  25-28
 h1  f1  d1  b1    29-32

position
fen

 01  02  03  04
05  06  07  08
 09  10  11  12
13  14  15  16
 17  18  19  20
21  22  23  24
 25  26  27  28
29  30  31  32

[FEN "W:W18,21,22,23,24,25,26,27,29,30,31,32:B1,2,3,5,6,7,8,9,10,11,12,28"]

*/

TEST_CASE("FEN W:Wh1", "Test 1") {
  FEN fen("FEN W:W29");

  Generator generator;
  generator.set(fen);
  std::vector<FEN> endPoss = generator.get();
  std::vector<FEN> etalonEndPoss{FEN("FEN B:W25")};

  REQUIRE(etalonEndPoss == endPoss);
}
