#ifndef GENERATOR_H
#define GENERATOR_H

#include <vector>

#include "FEN.h"

class Generator {
 public:
  Generator() = default;
  explicit Generator(const FEN& fen);

  void set(const FEN& fen);
  std::vector<FEN> get() const;

 private:
  FEN fen_;

 private:
  void parse();
};

#endif  // GENERATOR_H
