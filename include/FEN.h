#ifndef FEN_H
#define FEN_H

#include <string>

class FEN {
 public:
  FEN() = default;
  explicit FEN(const std::string& pos);

  void set(const std::string& pos);
  std::string get() const;

 private:
  std::string pos_;
};

inline bool operator==(const FEN& left, const FEN& right) {
  return left.get() == right.get();
}

inline bool operator!=(const FEN& left, const FEN& right) {
  return !(left == right);
}

#endif  // FEN_H
