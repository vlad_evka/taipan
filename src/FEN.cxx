#include "FEN.h"

FEN::FEN(const std::string& pos) : pos_(pos) {}

void FEN::set(const std::string& pos) {
  pos_ = pos;
}

std::string FEN::get() const {
  return pos_;
}
